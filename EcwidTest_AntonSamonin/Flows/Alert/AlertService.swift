//
//  AlertService.swift
//  EcwidTest_AntonSamonin
//
//  Created by Anton Samonin on 5/15/19.
//  Copyright © 2019 AntonSamonin. All rights reserved.
//

import UIKit

class AlertService {
    
    func alert(title: String, buttonTitle: String, name: String? = nil, price: String? = nil, quantity: String? = nil, imageData: Data? = nil,  completion: @escaping (([String?], Data?) -> Void)) -> AlertViewController {
        
        let storyboard = UIStoryboard(name: "AlertStoryboard", bundle: .main)
        let alertVC = storyboard.instantiateViewController(withIdentifier: "AlertVC") as! AlertViewController;
        
        alertVC.alertTitle = title
        alertVC.applyButtonTitle = buttonTitle
        alertVC.buttonAction = completion
        alertVC.oldProductName = name
        alertVC.oldProductPrice = price
        alertVC.oldProductQuantity = quantity
        alertVC.productImageData = imageData
        
        return alertVC
    }
    
}

