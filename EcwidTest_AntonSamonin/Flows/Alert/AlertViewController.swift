//
//  AlertViewController.swift
//  EcwidTest_AntonSamonin
//
//  Created by Anton Samonin on 5/15/19.
//  Copyright © 2019 AntonSamonin. All rights reserved.
//

import UIKit
import RealmSwift
import Photos

//Контроллер ввода данных о товаре
//для добавления товара в список
class AlertViewController: UIViewController  {
    
    var alertTitle: String?
    var applyButtonTitle: String?
    var productImageData: Data?
    var oldProductName: String?
    var oldProductPrice: String?
    var oldProductQuantity: String?
    var buttonAction: (([String?], Data?) -> Void)?
    
    let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var productNameTextField: UITextField!
    @IBOutlet weak var productPriceTextField: UITextField!
    @IBOutlet weak var productQuantityTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var addImageButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextField()
        configureTapGesture()
        setupView()
        addObservers()
        imagePicker.delegate = self
    }
    
    deinit {
        removeObservers()
    }
    
    @objc func handleTap() {
        view.endEditing(true)
    }
    
    // MARK: - Сonfigurations
    
    private func configureTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AlertViewController.handleTap))
        view.addGestureRecognizer(tapGesture)
    }
    
    private func configureTextField() {
        productNameTextField.delegate = self
        productPriceTextField.delegate = self
        productQuantityTextField.delegate = self
        
        productPriceTextField.keyboardType = UIKeyboardType.decimalPad
        productQuantityTextField.keyboardType = UIKeyboardType.decimalPad
    }
    
    private func setupView() {
        self.titleLabel.text = alertTitle
        self.applyButton.setTitle(applyButtonTitle, for: .normal)
        if let oldProductName = self.oldProductName, let oldProductPrice = oldProductPrice, let oldProductQuantity = oldProductQuantity, let oldProductImageData = productImageData {
            self.productNameTextField.text = oldProductName
            self.productPriceTextField.text = oldProductPrice
            self.productQuantityTextField.text = oldProductQuantity
            self.productImageData = oldProductImageData
        }
    }
    
    // MARK: - Button actions
    
    @IBAction func addProductImageButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        checkPermissionAndShowImagePicker()
    }
    
    @IBAction func applyButtonTapped(_ sender: UIButton) {
        view.endEditing(true)
        guard let productName = productNameTextField.text, let productPrice = productPriceTextField.text, let productQuantity = productQuantityTextField.text, let productImageData = productImageData, !productName.isEmpty, !productPrice.isEmpty, !productQuantity.isEmpty else {
            let alert = UIAlertController(title: nil, message: "Заполните все поля и добавьте изображение.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            present(alert, animated: true)
            return
        }
        buttonAction?([productName,productPrice,productQuantity], productImageData)
        let name = Notification.Name(rawValue: "tableReloadData")
        NotificationCenter.default.post(name: name, object: nil)
        dismiss(animated: true)
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    // MARK: - Keyboard
    
    @objc func keyboardWillChange(notification: Notification) {
        guard let keyBoardRect =  (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        if notification.name == UIResponder.keyboardWillShowNotification || notification.name == UIResponder.keyboardWillChangeFrameNotification {
            view.frame.origin.y = -keyBoardRect.height/2
        } else {
            view.frame.origin.y = 0
        }
    }
    
    // MARK: - Notification Observers
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    func removeObservers() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
}

// MARK: - TextField

extension AlertViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == productNameTextField {
            return true
        } else {
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
    }
}

// MARK: - UIImagePicker

extension AlertViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            productImageData = image.jpegData(compressionQuality: 0.5)
        }
        imagePicker.dismiss(animated: true, completion: nil)
        addImageButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        addImageButton.isEnabled = false
        addImageButton.setTitle("Изображение добавлено", for: .normal)
        addImageButton.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: .normal)
    }
    
    private func checkPermissionAndShowImagePicker() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            showImagePicker()
            print("Access is granted by user")
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ [weak self]
                (newStatus) in
                print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    self?.showImagePicker()
                    print("success")
                }
            })
            print("It is not determined until now")
        case .restricted:
            print("User do not have access to photo album.")
        case .denied:
            print("User has denied the permission.")
        @unknown default:
            fatalError()
        }
    }
    
    private func showImagePicker() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            present(self.imagePicker, animated: true, completion: nil)
        }
    }
}

