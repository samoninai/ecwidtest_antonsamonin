//
//  ProductDescriptionViewController.swift
//  EcwidTest_AntonSamonin
//
//  Created by Anton Samonin on 5/14/19.
//  Copyright © 2019 AntonSamonin. All rights reserved.
//

import UIKit
import RealmSwift

class ProductDescriptionViewController: UIViewController {
    
    var product: Product?
    var alertService = AlertService()
    
    // MARK: - Outlets
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productQuantityLabel: UILabel!
    @IBOutlet weak var deleteProductButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var editProductButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView(with: product)
    }
    
    // MARK: - Сonfigurations
    
    func configureView(with product: Product?) {
        if let product = product {
            self.productImageView.image = UIImage(data: product.image)
            self.productNameLabel.text = product.name
            self.productPriceLabel.text = String(product.price)
            self.productQuantityLabel.text = String(product.quantity)
        }
        self.priceLabel.isHidden = product == nil ? true : false
        self.quantityLabel.isHidden = product == nil ? true : false
        self.productNameLabel.isHidden = product == nil ? true : false
        self.productPriceLabel.isHidden = product == nil ? true : false
        self.productQuantityLabel.isHidden = product == nil ? true : false
        self.editProductButton.isEnabled = product == nil ? false : true
        self.deleteProductButton.isEnabled = product == nil ? false : true
        self.deleteProductButton.isHidden = product == nil ? true : false
    }
    
    // MARK: - Button Actions
    
    @IBAction func deleteProductButtonTapped(_ sender: UIButton) {
        
        if let product = self.product {
            DataBaseService.deleteObjectFromRealm(Product.self, objectPrimaryKey: product.id, config: DataBaseService.configuration)
            
            self.priceLabel.isHidden = true
            self.quantityLabel.isHidden = true
            
            self.deleteProductButton.setTitle("Товар удален", for: .normal)
            self.deleteProductButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            self.deleteProductButton.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            self.deleteProductButton.isEnabled = false
            self.editProductButton.isEnabled = false
            
            self.productImageView.image = nil
            self.productNameLabel.text = nil
            self.productPriceLabel.text = nil
            self.productQuantityLabel.text = nil
            
            let name = Notification.Name(rawValue: "tableReloadData")
            NotificationCenter.default.post(name: name, object: nil)
        }
    }
    
    @IBAction func editProductButtonTapped(_ sender: UIBarButtonItem) {
        
        if let product = self.product {
            let alert = alertService.alert(title: "Изменить товар", buttonTitle: "Применить", name: product.name, price: String(product.price), quantity: String(product.quantity), imageData: product.image) {
                [weak self] (productDetails, productImage) in
                
                if let productName = productDetails[0], let productPrice = productDetails[1], let productQuantity = productDetails[2], let productImage = productImage, let product = self?.product {
                    
                    let modifiedProduct = Product(name: productName, image: productImage, price: Int(productPrice)!, quantity: Int(productQuantity)!)
                    modifiedProduct.id = product.id
                    
                    DispatchQueue.global().sync {
                        let realm = DataBaseService.saveToRealm(items: [modifiedProduct], config: DataBaseService.configuration, update: true)
                        
                        self?.product = realm?.object(ofType: Product.self, forPrimaryKey: modifiedProduct.id)
                    }
                    self?.configureView(with: self?.product)
                }
            }
            present(alert, animated: true)
        }
    }
}
