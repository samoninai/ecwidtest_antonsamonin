//
//  ProductListViewController.swift
//  EcwidTest_AntonSamonin
//
//  Created by Anton Samonin on 5/14/19.
//  Copyright © 2019 AntonSamonin. All rights reserved.
//

import UIKit
import RealmSwift

class ProductListViewController: UITableViewController {
    
    private var productDescriptionViewController: ProductDescriptionViewController? = nil
    private var products: Results<Product>?
    
    let alertService = AlertService()
    let reloadDataName = Notification.Name("tableReloadData")
    
    @IBOutlet weak var productSearchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureSearchBar()
        createObservers()
        
        if let split = splitViewController {
            let controllers = split.viewControllers
            productDescriptionViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? ProductDescriptionViewController
        }
        
        self.products = DataBaseService.getObjectsFromRealm(Product.self, config: DataBaseService.configuration)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Button Action
    
    @IBAction func addProductButtonTapped(_ sender: UIBarButtonItem) {
        let alertVC = alertService.alert(title: "Добавить товар", buttonTitle: "Добавить") {
            [weak self] (productDetails, productImage) in
            
            if let productName = productDetails[0], let productPrice = productDetails[1], let productQuantity = productDetails[2], let productImage = productImage {
                
                let newProduct = Product(name: productName, image: productImage, price: Int(productPrice)!, quantity: Int(productQuantity)!)
                DispatchQueue.global().sync {
                    DataBaseService.saveToRealm(items: [newProduct], config: DataBaseService.configuration, update: true)
                    print("Товар сохранен")
                }
                self?.tableView.reloadData()
            }
        }
        present(alertVC, animated: true)
    }
    
    // MARK: - Notification Observers
    
    func createObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(ProductListViewController.reloadTableViewData), name: reloadDataName, object: nil)
    }
    
    @objc func reloadTableViewData() {
        tableView.reloadData()
    }
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let product = products?[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! ProductDescriptionViewController
                controller.product = product
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }
    
    // MARK: - Table View
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductListCell", for: indexPath) as? ProductListCell, let product = products?[indexPath.row] else {
            return UITableViewCell()
        }
        cell.configure(with: product)
        return cell
    }
    
}

// MARK: - Search Bar

extension ProductListViewController: UISearchBarDelegate {
    
    private func configureSearchBar() {
        productSearchBar.delegate = self
        productSearchBar.placeholder = "Поиск товара"
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.products = searchText.isEmpty ? self.products?.realm?.objects(Product.self) : self.products?.realm?.objects(Product.self).filter("name BEGINSWITH[c] %@", searchText)
        
        tableView.reloadData()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        products = products?.realm?.objects(Product.self)
        tableView.reloadData()
    }
    
}

