//
//  ProductListCell.swift
//  EcwidTest_AntonSamonin
//
//  Created by Anton Samonin on 5/14/19.
//  Copyright © 2019 AntonSamonin. All rights reserved.
//

import UIKit

class ProductListCell: UITableViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productQuantityLabel: UILabel!
    
    public func configure(with product: Product) {
        productImageView.image = UIImage(data: product.image)
        productNameLabel.text = product.name
        productPriceLabel.text = String(product.price)
        productQuantityLabel.text = String(product.quantity)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.productImageView.image = nil
        self.productNameLabel.text =  nil
        self.productPriceLabel.text = nil
        self.productQuantityLabel.text = nil
    }
    
}
