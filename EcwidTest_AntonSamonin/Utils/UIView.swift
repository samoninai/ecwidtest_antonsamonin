//
//  UIView.swift
//  EcwidTest_AntonSamonin
//
//  Created by Anton Samonin on 5/20/19.
//  Copyright © 2019 AntonSamonin. All rights reserved.
//

import UIKit

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        } get {
            return layer.cornerRadius
        }
    }
}

