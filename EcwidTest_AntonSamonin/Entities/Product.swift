//
//  Product.swift
//  EcwidTest_AntonSamonin
//
//  Created by Anton Samonin on 5/15/19.
//  Copyright © 2019 AntonSamonin. All rights reserved.
//

import Foundation
import RealmSwift

class Product: Object {
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var image = Data()
    @objc dynamic var price = 0
    @objc dynamic var quantity = 0
    
    convenience init(name: String, image: Data, price: Int, quantity: Int) {
        self.init()
        self.id = UUID().uuidString
        self.name = name
        self.image = image
        self.price = price
        self.quantity = quantity
    }

    override static func primaryKey() -> String? {
        return "id"
    }
    
    override static func indexedProperties() -> [String] {
        return ["name"]
    }
}
